<?php

namespace App\Domain\EOM_Zadania;

class FibonacciCalculator
{
    public function calculateFibonacci(int $count): int
    {
        if ($count < 1) {
            return 0;
        }

        if ($count == 1){
            return 1;
        }

        $number1 = 1;
        $number2 = 0;

        for ($i = 1; $i < $count; $i++) {
            $tmp = $number2;
            $number2 = $number1;
            $number1 += $tmp;
        }

        return $number1;
    }
}