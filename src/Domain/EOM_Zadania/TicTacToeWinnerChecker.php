<?php

namespace App\Domain\EOM_Zadania;

class TicTacToeWinnerChecker
{
    public function checkWinner($board): ?array
    {
        $lines = [
            [[$board[0][0], [0, 0]], [$board[0][1], [0, 1]], [$board[0][2], [0, 2]]],
            [[$board[1][0], [1, 0]], [$board[1][1], [1, 1]], [$board[1][2], [1, 2]]],
            [[$board[2][0], [2, 0]], [$board[2][1], [2, 1]], [$board[2][2], [2, 2]]],

            [[$board[0][0], [0, 0]], [$board[1][0], [1, 0]], [$board[2][0], [2, 0]]],
            [[$board[0][1], [0, 1]], [$board[1][1], [1, 1]], [$board[2][1], [2, 1]]],
            [[$board[0][2], [0, 2]], [$board[1][2], [1, 2]], [$board[2][2], [2, 2]]],

            [[$board[0][0], [0, 0]], [$board[1][1], [1, 1]], [$board[2][2], [2, 2]]],
            [[$board[2][0], [2, 0]], [$board[1][1], [1, 1]], [$board[0][2], [0, 2]]]
        ];

        foreach ($lines as $line) {
            if ($line[0][0] !== '' && $line[0][0] === $line[1][0] && $line[1][0] === $line[2][0]) {
                return [
                    'winner' => $line[0][0],
                    'winningSquares' => [$line[0][1], $line[1][1], $line[2][1]]
                ];
            }
        }

        return null;
    }
}