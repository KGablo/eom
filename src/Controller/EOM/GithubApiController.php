<?php

namespace App\Controller\EOM;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class GithubApiController extends AbstractController
{
    private HttpClientInterface $httpClient;

    public function __construct(HttpClientInterface $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    /** @Route("/repoAutocomplete/{repoName}", name="repoAutocomplete") */
    public function repoAutocomplete(string $repoName = null): JsonResponse
    {
        if (!$repoName) {
            return new JsonResponse(['items' => []]);
        }

        $response = $this->httpClient->request('GET', 'https://api.github.com/search/repositories', [
            'query' => [
                'q' => $repoName
            ]
        ]);

        return $this->json($response->toArray()['items']);
    }
}