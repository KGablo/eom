<?php

namespace App\Controller\EOM;

use App\Domain\EOM_Zadania\FibonacciCalculator;
use App\Domain\EOM_Zadania\TextCharacterFinder;
use App\Form\TaskOneType;
use App\Form\TaskThreeType;
use App\Form\TaskTwoType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EOM_TasksController extends AbstractController
{
    private TextCharacterFinder $textCharacterFinder;

    public function __construct(TextCharacterFinder $textCharacterFinder)
    {
        $this->textCharacterFinder = $textCharacterFinder;
    }

    /** @Route("/task1", name="task1") */
    public function task1(Request $request): Response
    {
        $submitted = false;
        $form = $this->createForm(TaskOneType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $submitted = true;
            $data = $form->getData();
            $result = $this->textCharacterFinder->findWordsAndCheckOrder($data['words'], $data['text']);
        }

        return $this->render('EOM/task1.html.twig', [
            'form' => $form->createView(),
            'result' => $result ?? null,
            'submitted' => $submitted,
        ]);
    }

    /** @Route("/task2", name="task2") */
    public function task2(Request $request): Response
    {
        $submitted = false;
        $form = $this->createForm(TaskTwoType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $submitted = true;
            $data = $form->getData();
            $result = $this->textCharacterFinder->findRepeatedCharacter2($data['signs']);
        }

        return $this->render('EOM/task2.html.twig', [
            'form' => $form->createView(),
            'result' => $result ?? null,
            'submitted' => $submitted,
        ]);
    }

    /** @Route("/task3", name="task3") */
    public function task3(Request $request, FibonacciCalculator $fibonacciCalculator): Response
    {
        $submitted = false;
        $form = $this->createForm(TaskThreeType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $submitted = true;
            $data = $form->getData();
            $result = $fibonacciCalculator->calculateFibonacci($data['count']);
        }

        return $this->render('EOM/task3.html.twig', [
            'form' => $form->createView(),
            'result' => $result ?? null,
            'submitted' => $submitted,
        ]);
    }

    /** @Route("/task5", name="task5") */
    public function task5(): Response
    {
        return $this->render('EOM/task5.html.twig', []);
    }

    /** @Route("/task6", name="task6") */
    public function task6(Request $request): Response
    {
        $submitted = false;
        $form = $this->createForm(TaskTwoType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $submitted = true;
            $data = $form->getData();
            $result = $this->textCharacterFinder->clickLikeOldPhone($data['signs']);
        }

        return $this->render('EOM/task6.html.twig', [
            'form' => $form->createView(),
            'result' => $result ?? null,
            'submitted' => $submitted,
        ]);
    }
}
