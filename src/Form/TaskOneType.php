<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TaskOneType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
//        unset($options);

        $builder
            ->add('text', TextareaType::class, [
                'label' => 'Tekst',
            ])
            ->add('words', CollectionType::class, [
                'entry_type' => TextType::class, // Pole typu TextType dla każdego elementu kolekcji
                'allow_add' => true, // Pozwól na dynamiczne dodawanie elementów
                'allow_delete' => true, // Pozwól na dynamiczne usuwanie elementów
                'entry_options' => [ // Opcje dla pól w kolekcji
                    'label' => false, // Ukryj etykietę dla pojedynczych pól
                ],
                'required' => true, // Opcjonalne pole
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Oblicz',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([]);
    }
}