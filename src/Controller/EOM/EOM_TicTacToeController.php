<?php

namespace App\Controller\EOM;

use App\Domain\EOM_Zadania\TicTacToeWinnerChecker;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EOM_TicTacToeController extends AbstractController
{
    /** @Route("/tictactoe", name="tictactoe") */
    public function tictactoe(Request $request, TicTacToeWinnerChecker $winnerChecker): Response
    {
        $board = $request->getSession()->get('board', [['', '', ''], ['', '', ''], ['', '', '']]);
        $currentPlayer = $request->getSession()->get('currentPlayer', 'X');
        $winningSquares = null;
        $winner = null;

        if ($request->isMethod('POST')) {
            $row = $request->request->get('row');
            $col = $request->request->get('col');

            if ($board[$row][$col] === '') {
                $board[$row][$col] = $currentPlayer;
                $currentPlayer = $currentPlayer === 'X' ? 'O' : 'X';
            }

            $request->getSession()->set('board', $board);
            $request->getSession()->set('currentPlayer', $currentPlayer);

            if ($winnerData = $winnerChecker->checkWinner($board)) {
                $winningSquares = $winnerData['winningSquares'];
                $winner = $winnerData['winner'];
            }
        }

        return $this->render('EOM/task4_game.html.twig', [
            'board' => $board,
            'currentPlayer' => $currentPlayer,
            'winner' => $winner,
            'winningSquares' => $winningSquares,
        ]);
    }

    /** @Route("/resetGame", name="resetGame") */
    public function resetGame(Request $request): Response
    {
        $session = $request->getSession();
        $session->remove('board');
        $session->remove('currentPlayer');

        return $this->redirectToRoute('tictactoe');
    }
}