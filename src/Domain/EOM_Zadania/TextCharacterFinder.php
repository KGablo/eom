<?php

namespace App\Domain\EOM_Zadania;

class TextCharacterFinder
{
    private array $keyMap = [
        'A' => '22', 'B' => '222', 'C' => '2222',
        'D' => '33', 'E' => '333', 'F' => '3333',
        'G' => '44', 'H' => '444', 'I' => '4444',
        'J' => '55', 'K' => '555', 'L' => '5555',
        'M' => '66', 'N' => '666', 'O' => '6666',
        'P' => '77', 'Q' => '777', 'R' => '7777', 'S' => '77777',
        'T' => '88', 'U' => '888', 'V' => '8888',
        'W' => '99', 'X' => '999', 'Y' => '9999', 'Z' => '99999',
        ' ' => '##'
    ];

    public function findWordsAndCheckOrder(array $words, string $text): string
    {
        $words = array_map('strtolower', $words);
        $text = strtolower($text);
        if ($this->findWordsInText($words, $text)) {
            $text = str_replace(['.', ','], '', $text);
            if (str_contains($text, implode(' ', $words))) {
                return 'Słowa występują w tekście i są zachowane w odpowiedniej kolejności.';
            }
            return 'Słowa występują w tekście bez zachowanej kolejności.';
        }

        return 'Słowa nie występują w tekście';
    }

    // Szuka w "tył"
    public function findRepeatedCharacter(string $someText): ?string
    {
        $charCounts = [];

        for ($i = 0; $i < strlen($someText); $i++) {
            $char = $someText[$i];

            if (isset($charCounts[$char])) {
                return $char;
            }

            $charCounts[$char] = 1;
        }

        return null;
    }

    // Szuka w "przód"
    public function findRepeatedCharacter2(string $input): ?string
    {
        $chars = str_split($input);
        $counts = array_count_values($chars);

        foreach ($chars as $char) {
            if ($counts[$char] > 1) {
                return $char;
            }
        }

        return null;
    }

    public function clickLikeOldPhone(string $text): string
    {
        $sequence = '';
        foreach (str_split(strtoupper($text)) as $char) {
            $sequence .= isset($this->keyMap[$char]) ? $this->keyMap[$char] . '-' : '(' . $char . ')-';
        }

        return trim($sequence, '-');
    }

    private function findWordsInText(array $words, string $text): bool
    {
        $textWords = preg_split('/[\s,.\']+/', $text);
        $totalWords = count($words);
        $wordIndex = 0;

        foreach ($textWords as $textWord) {
            if ($textWord == $words[$wordIndex]) {
                $wordIndex++;
            }

            if ($wordIndex === $totalWords) {
                return true;
            }
        }

        return false;
    }
}
