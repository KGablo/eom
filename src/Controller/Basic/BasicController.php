<?php

namespace App\Controller\Basic;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BasicController extends AbstractController
{
    /** @Route("/index", name="index") */
    public function index(): Response
    {

        return $this->render('index.html.twig', [
            'var' => 'var_test',
        ]);
    }
}